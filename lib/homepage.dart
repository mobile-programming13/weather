import 'package:flutter/material.dart';
import 'package:flutter_map/plugin_api.dart';
import 'package:latlong2/latlong.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  ScrollController? _scrollController;

  bool lastStatus = true;
  double height = 300;

  void _scrollListener() {
    if (_isShrink != lastStatus) {
      setState(() {
        lastStatus = _isShrink;
      });
    }
  }

  bool get _isShrink {
    return _scrollController != null &&
        _scrollController!.hasClients &&
        _scrollController!.offset > (height - kToolbarHeight);
  }

  @override
  void initState() {
    super.initState();
    _scrollController = ScrollController()..addListener(_scrollListener);
  }

  @override
  void dispose() {
    _scrollController?.removeListener(_scrollListener);
    _scrollController?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color.fromARGB(255, 76, 116, 175),
      body: CustomScrollView(
        controller: _scrollController,
        slivers: [
          appBar(),
          SliverList(
            delegate: SliverChildListDelegate(
              [
                hourForecast(),
                tomorrowTemp(),
                dayForecastTable(),
                sunEvent(),
                basicInformation(),
                map(),
                otherDetails(),
                const SizedBox(
                  height: 50,
                )
              ],
            ),
          )
        ],
      ),
    );
  }

  InkWell map() {
    return InkWell(
      onTap: () {},
      child: Container(
        margin: const EdgeInsets.symmetric(horizontal: 10, vertical: 2.5),
        padding: const EdgeInsets.symmetric(vertical: 20, horizontal: 20),
        decoration: const BoxDecoration(
          color: Color.fromARGB(85, 255, 255, 255),
          borderRadius: BorderRadius.all(
            Radius.circular(20),
          ),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const Text("Map"),
            const SizedBox(
              height: 10,
            ),
            ClipRRect(
              borderRadius: BorderRadius.circular(20),
              child: SizedBox(
                height: 200,
                child: FlutterMap(
                  options: MapOptions(
                    center: LatLng(13.2859406, 100.9233909),
                    zoom: 12,
                  ),
                  children: [
                    TileLayer(
                      urlTemplate:
                          'https://tile.openstreetmap.org/{z}/{x}/{y}.png',
                      userAgentPackageName: 'dev.fleaflet.flutter_map.example',
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget otherDetails() {
    return InkWell(
      onTap: () {},
      child: Container(
        margin: const EdgeInsets.symmetric(horizontal: 10, vertical: 2.5),
        padding: const EdgeInsets.symmetric(vertical: 15, horizontal: 20),
        decoration: const BoxDecoration(
          color: Color.fromARGB(85, 255, 255, 255),
          borderRadius: BorderRadius.all(
            Radius.circular(20),
          ),
        ),
        child: Table(
          defaultVerticalAlignment: TableCellVerticalAlignment.middle,
          columnWidths: const {
            0: FixedColumnWidth(30),
            1: FixedColumnWidth(200),
          },
          children: [
            TableRow(
              children: [
                TableCell(
                  child: Center(
                    child: Image.asset(
                      "assets/speed.png",
                      height: 25,
                    ),
                  ),
                ),
                TableCell(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: const [
                      SizedBox(
                        height: 40,
                      ),
                      Padding(
                        padding: EdgeInsets.only(left: 8.0),
                        child: Text("AQI"),
                      )
                    ],
                  ),
                ),
                TableCell(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: const [
                      Text("Moderate (82)"),
                    ],
                  ),
                ),
              ],
            ),
            TableRow(
              children: [
                TableCell(
                  child: Center(
                    child: Image.asset(
                      "assets/air-pollution.png",
                      height: 25,
                    ),
                  ),
                ),
                TableCell(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: const [
                      SizedBox(
                        height: 40,
                      ),
                      Padding(
                        padding: EdgeInsets.only(left: 8.0),
                        child: Text("Pollen"),
                      )
                    ],
                  ),
                ),
                TableCell(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: const [
                      Text("None"),
                    ],
                  ),
                ),
              ],
            ),
            TableRow(
              children: [
                TableCell(
                  child: Center(
                    child: Image.asset(
                      "assets/car.png",
                      height: 25,
                    ),
                  ),
                ),
                TableCell(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: const [
                      SizedBox(
                        height: 40,
                      ),
                      Padding(
                        padding: EdgeInsets.only(left: 8.0),
                        child: Text("Driving difficulty"),
                      )
                    ],
                  ),
                ),
                TableCell(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: const [
                      Text("None"),
                    ],
                  ),
                ),
              ],
            ),
            TableRow(
              children: [
                TableCell(
                  child: Center(
                    child: Image.asset(
                      "assets/athletics.png",
                      height: 25,
                    ),
                  ),
                ),
                TableCell(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: const [
                      SizedBox(
                        height: 40,
                      ),
                      Padding(
                        padding: EdgeInsets.only(left: 8.0),
                        child: Text("Running"),
                      )
                    ],
                  ),
                ),
                TableCell(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: const [
                      Text("Good"),
                    ],
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  Widget basicInformation() {
    return InkWell(
      onTap: () {},
      child: Container(
        margin: const EdgeInsets.symmetric(horizontal: 10, vertical: 2.5),
        padding: const EdgeInsets.all(10),
        height: 150,
        decoration: const BoxDecoration(
          color: Color.fromARGB(85, 255, 255, 255),
          borderRadius: BorderRadius.all(
            Radius.circular(20),
          ),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Column(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Image.asset(
                  "assets/sun.png",
                  fit: BoxFit.contain,
                  width: 50,
                ),
                const Text("UV index"),
                const Text("Low"),
              ],
            ),
            const VerticalDivider(
              thickness: 1,
              indent: 10,
              endIndent: 10,
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Image.asset(
                  "assets/humidity.png",
                  fit: BoxFit.contain,
                  width: 50,
                ),
                const Text("Humidity"),
                const Text("65%"),
              ],
            ),
            const VerticalDivider(
              thickness: 1,
              indent: 10,
              endIndent: 10,
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Image.asset(
                  "assets/wind.png",
                  fit: BoxFit.contain,
                  width: 50,
                ),
                const Text("Wind"),
                const Text("11 km/h"),
              ],
            )
          ],
        ),
      ),
    );
  }

  Widget sunEvent() {
    return InkWell(
      onTap: () {},
      child: Container(
        margin: const EdgeInsets.symmetric(horizontal: 10, vertical: 2.5),
        padding: const EdgeInsets.all(10),
        height: 130,
        decoration: const BoxDecoration(
          color: Color.fromARGB(85, 255, 255, 255),
          borderRadius: BorderRadius.all(
            Radius.circular(20),
          ),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Column(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                const Text("Sunrise"),
                const Text("6:39 AM"),
                Image.asset(
                  "assets/sunrise.png",
                  fit: BoxFit.contain,
                  width: 50,
                )
              ],
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                const Text("Sunset"),
                const Text("6:01 PM"),
                Image.asset(
                  "assets/sunset.png",
                  fit: BoxFit.contain,
                  width: 50,
                )
              ],
            )
          ],
        ),
      ),
    );
  }

  Widget dayForecastTable() {
    return InkWell(
      onTap: () {},
      child: Container(
        margin: const EdgeInsets.symmetric(horizontal: 10, vertical: 2.5),
        padding: const EdgeInsets.symmetric(vertical: 15, horizontal: 20),
        // height: 0,
        decoration: const BoxDecoration(
          color: Color.fromARGB(85, 255, 255, 255),
          borderRadius: BorderRadius.all(
            Radius.circular(20),
          ),
        ),
        child: Table(
          defaultVerticalAlignment: TableCellVerticalAlignment.middle,
          columnWidths: const {
            0: FixedColumnWidth(170),
          },
          children: [
            TableRow(
              children: [
                const TableCell(
                  child: Text(
                    "Yesterday",
                    style: TextStyle(color: Colors.black45),
                  ),
                ),
                TableCell(
                  child: Container(),
                ),
                TableCell(
                  child: Container(
                    height: 35,
                  ),
                ),
                TableCell(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: const [
                      Text(
                        "30°",
                        style: TextStyle(color: Colors.black45),
                      ),
                      Text(
                        "22°",
                        style: TextStyle(color: Colors.black45),
                      ),
                    ],
                  ),
                ),
              ],
            ),
            TableRow(
              children: [
                const TableCell(
                  child: Text("Today"),
                ),
                TableCell(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Image.asset(
                        "assets/humid.png",
                        height: 10,
                        color: Colors.black54,
                      ),
                      const SizedBox(
                        width: 10,
                      ),
                      const Text('1%'),
                    ],
                  ),
                ),
                TableCell(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Image.asset(
                        "assets/weathericon.png",
                        height: 35,
                      ),
                    ],
                  ),
                ),
                TableCell(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: const [
                      Text("30°"),
                      Text("22°"),
                    ],
                  ),
                ),
              ],
            ),
            TableRow(
              children: [
                const TableCell(
                  child: Text("Wednesday"),
                ),
                TableCell(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Image.asset(
                        "assets/humid.png",
                        height: 10,
                        color: Colors.black54,
                      ),
                      const SizedBox(
                        width: 10,
                      ),
                      const Text('1%'),
                    ],
                  ),
                ),
                TableCell(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Image.asset(
                        "assets/weathericon.png",
                        height: 35,
                      ),
                    ],
                  ),
                ),
                TableCell(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: const [
                      Text("31°"),
                      Text("23°"),
                    ],
                  ),
                ),
              ],
            ),
            TableRow(
              children: [
                const TableCell(
                  child: Text("Thursday"),
                ),
                TableCell(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Image.asset(
                        "assets/humid.png",
                        height: 10,
                        color: Colors.black54,
                      ),
                      const SizedBox(
                        width: 10,
                      ),
                      const Text('1%'),
                    ],
                  ),
                ),
                TableCell(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Image.asset(
                        "assets/weathericon.png",
                        height: 35,
                      ),
                    ],
                  ),
                ),
                TableCell(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: const [
                      Text("31°"),
                      Text("22°"),
                    ],
                  ),
                ),
              ],
            ),
            TableRow(
              children: [
                const TableCell(
                  child: Text("Friday"),
                ),
                TableCell(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Image.asset(
                        "assets/humid.png",
                        height: 10,
                        color: Colors.black54,
                      ),
                      const SizedBox(
                        width: 10,
                      ),
                      const Text('3%'),
                    ],
                  ),
                ),
                TableCell(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Image.asset(
                        "assets/weathericon.png",
                        height: 35,
                      ),
                    ],
                  ),
                ),
                TableCell(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: const [
                      Text("29°"),
                      Text("22°"),
                    ],
                  ),
                ),
              ],
            ),
            TableRow(
              children: [
                const TableCell(
                  child: Text("Saturday"),
                ),
                TableCell(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Image.asset(
                        "assets/humid.png",
                        height: 10,
                        color: Colors.black54,
                      ),
                      const SizedBox(
                        width: 10,
                      ),
                      const Text('1%'),
                    ],
                  ),
                ),
                TableCell(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Image.asset(
                        "assets/weathericon.png",
                        height: 35,
                      ),
                    ],
                  ),
                ),
                TableCell(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: const [
                      Text("27°"),
                      Text("22°"),
                    ],
                  ),
                ),
              ],
            ),
            TableRow(
              children: [
                const TableCell(
                  child: Text("Sunday"),
                ),
                TableCell(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Image.asset(
                        "assets/humid.png",
                        height: 10,
                        color: Colors.black54,
                      ),
                      const SizedBox(
                        width: 10,
                      ),
                      const Text('0%'),
                    ],
                  ),
                ),
                TableCell(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Image.asset(
                        "assets/weathericon.png",
                        height: 35,
                      ),
                    ],
                  ),
                ),
                TableCell(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: const [
                      Text("29°"),
                      Text("23°"),
                    ],
                  ),
                ),
              ],
            ),
            TableRow(
              children: [
                const TableCell(
                  child: Text("Monday"),
                ),
                TableCell(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Image.asset(
                        "assets/humid.png",
                        height: 10,
                        color: Colors.black54,
                      ),
                      const SizedBox(
                        width: 10,
                      ),
                      const Text('0%'),
                    ],
                  ),
                ),
                TableCell(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Image.asset(
                        "assets/weathericon.png",
                        height: 35,
                      ),
                    ],
                  ),
                ),
                TableCell(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: const [Text("30°"), Text("24°")],
                  ),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }

  Widget tomorrowTemp() {
    return InkWell(
      onTap: () {},
      child: Container(
        margin: const EdgeInsets.symmetric(horizontal: 10, vertical: 2.5),
        padding: const EdgeInsets.all(10),
        height: 100,
        decoration: const BoxDecoration(
          color: Color.fromARGB(85, 255, 255, 255),
          borderRadius: BorderRadius.all(
            Radius.circular(20),
          ),
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: const [
            Text(
              "Tomorrow's Temperature",
              style: TextStyle(
                fontSize: 16.5,
              ),
            ),
            SizedBox(
              height: 5,
            ),
            Text(
              "Expect the same as today",
              style: TextStyle(
                fontSize: 15,
                color: Colors.black54,
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget hourForecast() {
    return InkWell(
      onTap: () {},
      child: Container(
        height: 150,
        margin: const EdgeInsets.symmetric(horizontal: 10, vertical: 2.5),
        padding: const EdgeInsets.all(10),
        decoration: const BoxDecoration(
          color: Color.fromARGB(85, 255, 255, 255),
          borderRadius: BorderRadius.all(
            Radius.circular(20),
          ),
        ),
        child: ListView.builder(
          itemCount: 10,
          scrollDirection: Axis.horizontal,
          itemBuilder: (context, index) => Container(
            width: 80,
            height: 140,
            // color: Colors.amber,
            margin: const EdgeInsets.symmetric(horizontal: 5),
            child: Column(
              children: [
                Text('${index + 1} PM'),
                const Spacer(),
                Image.asset(
                  "assets/weathericon.png",
                  height: 35,
                ),
                const Spacer(
                  flex: 1,
                ),
                Text('${28 + (index + 1) ~/ 3}°'),
                const Spacer(),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Image.asset(
                      "assets/humid.png",
                      height: 10,
                      color: Colors.black54,
                    ),
                    const SizedBox(
                      width: 10,
                    ),
                    const Text('1%'),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  SliverAppBar appBar() {
    return SliverAppBar(
      elevation: 0,
      backgroundColor: Colors.transparent,
      titleSpacing: 10,
      title: _isShrink
          ? Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: const [
                Icon(Icons.location_on),
                SizedBox(
                  width: 7,
                ),
                Text("Maung Saen Suk"),
              ],
            )
          : null,
      expandedHeight: 250,
      floating: false,
      pinned: true,
      snap: false,
      flexibleSpace: Container(
        height: 300,
        decoration: const BoxDecoration(
          borderRadius: BorderRadius.vertical(bottom: Radius.circular(20)),
          gradient: LinearGradient(
            // stops: [0.25, 0.8],
            begin: Alignment.topLeft,
            end: Alignment.bottomCenter,
            colors: [
              Color.fromARGB(255, 76, 145, 175),
              Color.fromARGB(255, 76, 116, 175)
            ],
          ),
        ),
        child: !_isShrink
            ? Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(left: 12.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: const [
                        Spacer(
                          flex: 3,
                        ),
                        Text(
                          "28°",
                          style: TextStyle(
                            fontSize: 50,
                            fontWeight: FontWeight.bold,
                            color: Colors.white,
                          ),
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        Text(
                          "Maung Saen Suk",
                          style: TextStyle(
                            fontSize: 25,
                            fontWeight: FontWeight.w200,
                            color: Colors.white,
                          ),
                        ),
                        Flexible(
                          child: Text(
                            "Chonburi, Thailand",
                            style: TextStyle(
                              fontSize: 18,
                              fontWeight: FontWeight.w200,
                              color: Colors.white,
                            ),
                          ),
                        ),
                        Spacer(
                          flex: 1,
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(right: 8.0),
                    child: Image.asset(
                      "assets/weatheranimation.gif",
                      height: 180,
                      fit: BoxFit.scaleDown,
                    ),
                  )
                ],
              )
            : null,
      ),
      actions: _isShrink
          ? [
              const SizedBox(
                width: 70,
                child: Center(
                  child: Text(
                    "28°",
                    style: TextStyle(
                      fontSize: 20,
                    ),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(right: 20.0),
                child: SizedBox(
                  width: 40,
                  child: Center(
                    child: Image.asset(
                      "assets/weatheranimation.gif",
                      height: 30,
                      fit: BoxFit.scaleDown,
                    ),
                  ),
                ),
              ),
            ]
          : null,
    );
  }
}
